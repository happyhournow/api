import Place from '../models/Place/Place'

const getNearest = async (maxDistance: number, center: Array<number>) => {
  try {
    const places = await Place.find()
    .where('location.coords').near(
      {
        center,
        maxDistance: maxDistance / 6371,
        spherical: true
      })
    return places
  } catch (e) {
    throw new Error(e)
  }
}

getNearest(1, [-0.1280845, 51.46419299999999])
.then((res) => {
  console.log(res)
})
.catch((err) => {
  console.log(err)
})