import mongoose from '../../db/mongoose'
import { Schema, Model } from 'mongoose'
import { UserTypes } from '../../compiler/types'
const config = require('../../config/config')

const userSchema: Schema = mongoose.Schema({
    email: {
      required: true,
      type: String,
      maxlength: 254,
      minlength: 7,
      unique: true,
      trim: true
    },
    screenName: {
      required: true,
      type: String,
      maxlength: 254,
      minlength: 3,
      trim: true,
      unique: true
    },
    edits: {
      type: Number,
      min: 0
    },
    created: {
      type: Number,
      min: 0
    },
    places: [{ type: Schema.Types.ObjectId }]
})

const User: Model<UserTypes.UserSchema> = mongoose.model('User', userSchema)

export default User