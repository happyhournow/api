import mongoose from '../../db/mongoose'
import { PlaceSchema } from '../../compiler/types'
import { Model, Schema } from 'mongoose'
const config = require('../../config/config')

const placeSchema: Schema = mongoose.Schema({
    name: {
        type: String,
        maxlength: 30,
        
        index: true,
        trim: true
    },
    googlePlaceId: {
        type: String,
        
        unique: true
    },
    location: {
        string: { type: String, required: true },
        coords: {
            lng: { type: Number,  unique: true },
            lat: { type: Number,  unique: true }
        },
     },
    timezone: { type: String, required: true },
    type: {
        type: String,
        maxlength: 30,
        trim: true
    },
    menu: {
        type: String,
        minlength: 10,
        trim: true
    },
    description: {
        type: String,
        trim: true
    },
    rating: {
        type: Number,
        max: 5,
        min: 1
    },
    price: {
        type: Number,
        max: 5,
        min: 1
    },
    creator: { type: String, default: 'anon' },
    days: {
        0: {
            start: { type: Number, max: 23, min: 0 },
            end: { type: Number, max: 23, min: 0 },
            timeString: { type: String, minlength: 2, maxlength: 40 },
            deal: {
                type: String,
                
                maxlength: 60,
                trim: true
            }
        },
        1: {
            start: { type: Number, max: 23, min: 0 },
            end: { type: Number, max: 23, min: 0 },
            timeString: { type: String, minlength: 2, maxlength: 40 },
            deal: {
                type: String,
                
                maxlength: 60,
                trim: true
            }
        },
        2: {
            start: { type: Number, max: 23, min: 0 },
            end: { type: Number, max: 23, min: 0 },
            timeString: { type: String, minlength: 2, maxlength: 40 },
            deal: {
                type: String,
                
                maxlength: 60,
                trim: true
            }
        },
        3: {
            start: { type: Number, max: 23, min: 0 },
            end: { type: Number, max: 23, min: 0 },
            timeString: { type: String, minlength: 2, maxlength: 40 },
            deal: {
                type: String,
                
                maxlength: 60,
                trim: true
            }
        },
        4: {
            start: { type: Number, max: 23, min: 0 },
            end: { type: Number, max: 23, min: 0 },
            timeString: { type: String, minlength: 2, maxlength: 40 },
            deal: {
                type: String,
                
                maxlength: 60,
                trim: true
            }
        },
        5: {
            start: { type: Number, max: 23, min: 0 },
            end: { type: Number, max: 23, min: 0 },
            timeString: { type: String, minlength: 2, maxlength: 40 },
            deal: {
                type: String,
                
                maxlength: 60,
                trim: true
            }
        },
        6: {
            start: { type: Number, max: 23, min: 0 },
            end: { type: Number, max: 23, min: 0 },
            timeString: { type: String, minlength: 2, maxlength: 40 },
            deal: {
                type: String,
                
                maxlength: 60,
                trim: true
            }
        }
    }

})

placeSchema.index({ 'location.coords': '2dsphere'})

const Place: Model<PlaceSchema> = mongoose.model('Place', placeSchema)

const newPlace: PlaceSchema = new Place({
    name: 'The Mill',
    location: {
        string: 'Ulverston',
        coords: {
            lat: 54.1971476,
            lng: -3.0960794
        }
    },
    googlePlaceId: 'ChIJrUvEYpikfEgR0WwqZYdTIq0',
    timezone: 'GMT',
    creator: 'theomjones',
    days: {
        5: {
            start: 18,
            end: 20,
            deal: '241 Shots',
            timeString: '6pm - 8pm'
        }
    }

})

// newPlace.save()
// .then((doc) => {
//     console.log('saved: ', doc)
// })
// .catch((e) => {
//     console.log(e)
// })

export default Place