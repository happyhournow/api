import Place from '../Place'
import { PlaceSchema, Geo } from '../../../compiler/types'
import getDistance from '../../../Services/DistanceServices/GetDistance'

type Longitude = number
type Latitude = number

/*
******** geoNear is a variation of find().near() it allows pulling the dist data off the object. For pagination.
1) getDistanceGeo() finds all the documents which are close with the provided geoJSON.
2) It turns the BSON object into a real js object, it pulls the coord objects off this, pushing it to an array
3) We call getDistance, passing in the array of Coord objects
4) We modify the find result to include distance and duration which gets returned by getDistance
4) _Return the modified result_
**
Use if location is being used in app.
******** IT MUST TAKE CENTER as LONG,LAT.
*/

// (''metric', 'walking', { lat: 34,0349890, lng: -0.3489278 }, 200<metres>)
const getNearestSpatial = async (unit: Geo.Unit, mode: Geo.Mode, origin: Geo.Coords, maxDistance: number) => {
  const lng: Longitude = origin.lng
  const lat: Latitude = origin.lat
  const places: Array<PlaceSchema> = []
  maxDistance *= 6371
  try {
    const placeList: Array<PlaceSchema> = await Place.geoNear({
      type: 'Point', coordinates: [lng, lat]
    }, {
      spherical: true,
      maxDistance,
      distanceField: 'mongodist'
    }) as Array<PlaceSchema>

    // For now using any, because problems with ts compiler and the parsed BSON Obj.
    // parse BSON into string, then parse string into json...
    const flatPlaces: Array<any> = JSON.parse(JSON.stringify(placeList))
    const placeCoords: Array<Geo.Coords> = []

    if (flatPlaces.length > 0) {

      flatPlaces.map((place: any) => {
        const coords: Geo.Coords = place.obj.location.coords
        placeCoords.push(coords)
      })

      const distances: Array<Geo.DistanceObject> = await getDistance(unit, mode, origin, placeCoords)
      flatPlaces.map((place, i) => {
        place.obj.distance = distances[i].distance
        place.obj.duration = distances[i].duration
      })

      // Strip the 'dist' key/value given by collection.geoNear()
        // Push it to the places array at the top.
      flatPlaces.map((place) => {
        places.push(place.obj)
      })

    } else {
      return ['Could not find any places in that range.']
    }

    // Return the stripped down array without 'dist'
    return places
  } catch (e) {
    console.log(e)
    throw new Error(e)
  }
}

export default getNearestSpatial