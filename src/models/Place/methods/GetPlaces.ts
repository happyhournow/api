import getDistance from '../../../Services/DistanceServices/GetDistance'
import Place from '../Place'

// TYPES
import { PlaceSchema } from '../../../compiler/types'

/*
Simply finds all the places (optional paginate)
Returns results
*/

const GetPlaces = async (paginate?: boolean) => {
    try {
      const placeList: Array<PlaceSchema> = await Place.find() as Array<PlaceSchema>

      return placeList
    } catch (e) {
      throw new Error(e)
    }
}

export default GetPlaces
