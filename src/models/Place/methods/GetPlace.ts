import { PlaceSchema, Geo, PlaceObject } from '../../../compiler/types'

import Place from '../Place'
import getDistance from '../../../Services/DistanceServices/GetDistance'

const GetPlace = async (id: string, unit: Geo.Unit, mode: Geo.Mode, origin: Geo.Coords) => {
  let place: PlaceSchema
  if (unit && mode && origin) {
    try {
      place = await Place.findById(id).lean() as PlaceSchema
      const distances: Array<Geo.DistanceObject> = await getDistance(unit, mode, origin, [place.location.coords])
      place.distance = distances[0].distance
      place.duration = distances[0].duration
      return place
    } catch (e) {
      throw new Error(e)
    }
  } else {
    try {
      place = await Place.findById(id)
    } catch (e) {
      throw new Error(e)
    }
    return place
  }
}

export default GetPlace
