import Place from '../Place'
import { PlaceSchema, Geo } from '../../../compiler/types'
import getDistance from '../../../Services/DistanceServices/GetDistance'

type Longitude = number
type Latitude = number

/*
1) getNearest first finds the nearest places within a range (maxDistance)
2) It grabs all the location coords from this object
3) We call getDistance, passing in the array of Coord objects
4) We modify the find result to include distance and duration
4) Return the modified result
**
Use if location is being used in app.
******** IT MUST TAKE CENTER as LONG,LAT.
*/

const getNearest = async (unit: Geo.Unit, mode: Geo.Mode, maxDistance: number, coords: Geo.Coords) => {

  const center = [coords.lng, coords.lat]
  maxDistance /= 6371

  try {
    const placeList: Array<PlaceSchema> = await Place.find()
    .where('location.coords').near(
      {
        center,
        maxDistance,
        spherical: true
      }).lean() as Array<PlaceSchema>

    let placeCoords: Array<Geo.Coords> = []
    placeList.map((place: PlaceSchema) => {
      const coords: Geo.Coords = place.location.coords
      placeCoords = [...placeCoords, coords]
    })

    const distances: Array<Geo.DistanceObject> = await getDistance(unit, mode, coords, placeCoords)
    placeList.map((place, i) => {
      place.distance = distances[i].distance
      place.duration = distances[i].duration
    })

    return placeList
  } catch (e) {
    throw new Error(e)
  }
}

export default getNearest