import validate from 'mongoose-validator'

const screenNameValidator = [
  validate({
    validator: 'isLength'
  })
]