import { Document, Types } from 'mongoose'

export namespace Geo {
    export interface Coords {
        lat: number
        lng: number
      }

      export type Mode = 'walking' | 'driving' | 'transit'
      export type Unit = 'metric' | 'imperial'

      export interface Distance {
        distance: {
          text: string
          value: number
        }
      }

      export interface Duration {
        duration: {
          text: string
          value: number
        }
      }

      export interface DistanceObject {
        distance: Distance
        duration: Duration
      }
}

export namespace PlaceTypes {
    export interface Day {
        start: number
        end: number
        timeString: number
        deal: string
    }
}

export interface PlaceObject {
    name: string
    googlePlaceId: string
    location: {
        string: string
        coords: {
            lat: number
            lng: number
        }
    }
    timezone: string
    menu?: string
    description: string
    rating?: number
    price?: number
    creator: string
    distance?: Geo.Distance
    duration?: Geo.Duration

    days: {
      0?: {
          start: number
          end: number
          timeString: string
          deal: string
      },
      1?: {
          start: number
          end: number
          timeString: string
          deal: string
      },
      2?: {
          start: number
          end: number
          timeString: string
          deal: string
      },
      3?: {
          start: number
          end: number
          timeString: string
          deal: string
      },
      4?: {
          start: number
          end: number
          timeString: string
          deal: string
      },
      5?: {
          start: number
          end: number
          timeString: string
          deal: string
      },
      6?: {
          start: number
          end: number
          timeString: string
          deal: string
      }
  }
}

export interface PlaceSchema extends Document, PlaceObject { }

export namespace UserTypes {
    export interface UserSchema extends Document  {
        email: string,
        password: string,
        screenName: string,
        edits: number,
        created: number,
        places: Array<Types.ObjectId>
    }
    export interface PostUser {
        email: string,
        password: string,
        screenName: string
    }
}
