const axios = require('axios')

import { Geo } from '../../compiler/types'

/*
This would be better if it just accepted an array of documents, parsed them, and returned the documents
with the distances attached. Would make it way more reusable.
*/

const getDistance = async (units: Geo.Unit, mode: Geo.Mode, userLoc: Geo.Coords, destinations: Array<Geo.Coords>) => {
  const originLat: number = userLoc.lat
  const originLng: number = userLoc.lng
  // For google API url. format 'lat,lng'
  const origin: string = encodeURIComponent(`${originLat},${originLng}`)
  // Also for google API url || format 'lat,lng|lat,lng|lat,lng'
  let destinationsList: Array<string> = []
  destinations.forEach(dest => {
    destinationsList = [...destinationsList, (`${dest.lat},${dest.lng}`)]
  })
  const encodedDests: string = encodeURIComponent(destinationsList.join('|'))

  try {
    const res = await axios.get(`https://maps.googleapis.com/maps/api/distancematrix/json?units=${units}&mode=${mode}&origins=${origin}&destinations=${encodedDests}&key=AIzaSyC7C-vZ1a3jEGOQm1cQZgWsgL6SlHFk8VY`)
    const dists: Array<Geo.DistanceObject> = res.data.rows[0].elements

    return dists
  } catch (e) {
    console.error(e)
    throw new Error(e)
  }
}

export default getDistance