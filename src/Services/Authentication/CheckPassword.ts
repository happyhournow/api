import * as bcrypt from 'bcrypt'

const checkPassword = async (plainPassword: string, hashedPassword: string) => {
  try {
    const result = await bcrypt.compare(plainPassword, hashedPassword)
    return result
  } catch (e) {
    throw new Error(e)
  }
}

export default checkPassword