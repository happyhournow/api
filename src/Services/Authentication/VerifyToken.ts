import secret from '../../config/key'
import * as jwt from 'jsonwebtoken'

interface TokenReturn {
  uid: string,
  iat: number,
  exp: number
}

const verifyToken = async (token: string): Promise<TokenReturn> => {
  try {
    const decoded: any = await jwt.verify(token, secret)
    return decoded
  } catch (e) {
    const error: jwt.JsonWebTokenError = e
    throw e
  }
}

export default verifyToken