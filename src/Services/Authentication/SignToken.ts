import secret from '../../config/key'
import * as jwt from 'jsonwebtoken'

const signToken = async (userId?: string, short?: boolean) => {

  const payload = {
    uid: userId
  }
  const token = await jwt.sign(payload, secret, {
    expiresIn: short ? '5m' : '30 days'
  })
  return token
}

export default signToken