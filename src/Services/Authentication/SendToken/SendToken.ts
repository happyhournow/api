import { Response, Request } from 'express'
import * as nodemailer from 'nodemailer'

// Check if user exists.
    // If no, create user.
    // If yes, do not create user.
  // Generate token
  // Send token

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'theomjones@gmail.com',
    pass: 'glxfdpnyjqywwmpc'
  }
})

const sendToken = async (email: string, token: string) => {
  const mailOptions: nodemailer.SendMailOptions = {
    from: 'theomjones@gmail.com',
    to: email,
    subject: 'Sup dog 🎉',
    // Figure out when route is built
    html: `<html>
  <head>
    <style>
        .title {
          font-size: 10px;
          color: #2E294E;
        }
        .content {
          color: #333;
        }
    </style>
  </head>
  <body>
    <h1 class="title">Hey, your token has arrived.</h1>
    <div class="content">
      Sign in below! <br />
    h    <a href="http://localhost:9021/user/authenticate?token=${token}" style="padding: 5px 10px;
    border-radius: 3px;
    box-shadow: -1px 2px 2px rgba(0, 0,0,.4);
    color: white;
    background: rgb(68, 168, 66);">Sign In</a>
    </div>
  </body>
</html>
    `
  }
  try {
    const response: nodemailer.SentMessageInfo = await transporter.sendMail(mailOptions)
    return response
  } catch (e) {
    throw e
  }
}

export default sendToken