import sendToken from './SendToken'
import * as nodemailer from 'nodemailer'

test('It should correctly send an email, with token', () => {
  sendToken('theojones100@gmail.com', '1234')
  .then((res: nodemailer.SentMessageInfo) => {
    expect(res.accepted[0]).toEqual('theojones100@gmail.com')
  })
  .catch((e) => {
    fail()
  })
})