import * as bcrypt from 'bcrypt'

const saltRounds: number = 12

const hashPassword = async (plainPassword: string) => {
  try {
    const hash = await bcrypt.hash(plainPassword, saltRounds)
    return hash
  } catch (e) {
    throw new Error(e)
  }
}

export default hashPassword