import FindUserByEmail from './FindUserByEmail'
import { UserTypes } from '../../compiler/types'

test('Find user by email', async () => {
  try {
    const user = await FindUserByEmail('theomjones@gmail.com')
    expect(user.screenName).toEqual('theomjones')
  } catch (e) {
    fail(e)
  }
})