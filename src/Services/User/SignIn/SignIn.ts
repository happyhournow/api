import { Document } from 'mongoose'
import { UserTypes } from './../../../compiler/types'
import sendToken from '../../../Services/Authentication/SendToken/SendToken'
import findUserByEmail from '../../../Services/User/FindUserByEmail'
import signToken from '../../../Services/Authentication/SignToken'
import saveUser from '../../../Services/User/SaveUser'
import findUser from '../../../Services/User/FindUser/FindUser'

/*
  * Signs an existing user in.
*/

export class SignIn {
  private token: string
  public email: string
  public screenName: string
  private userID: string
  constructor(email: string, screenName?: string) {
    email ? this.email = email : this.email = undefined
    screenName ? this.screenName = screenName : this.screenName = undefined
  }

  async checkExisting () {
    try {
      const user: any = await findUser(this.email, this.screenName)
      this.userID = user._id
      if (user) {
        this.email = user.email
        return user
      } else {
        throw new Error('User does not exist')
      }
    } catch (e) {
      throw e
    }
  }

  async createNewUser () {
    try {
      const user = await this.checkExisting()
      throw new Error('User already exists.')
    } catch (e) {
      try {
        const user = await saveUser(this.email, this.screenName)
        return user
      } catch (e) {
        throw e
      }
    }
  }

  async signToken (uid) {
    try {
      const token = await signToken(uid, true)
      return token
    } catch (e) {
      throw e
    }
  }

  async sendToken (token) {
    try {
      const response = await sendToken(this.email, token)
      return response
    } catch (e) {
      throw e
    }
  }

}

const signIn = async (email?: string, screenName?: string) => {
  const newUser = new SignIn(email, screenName)

  return newUser.checkExisting()
  .then((user: UserTypes.UserSchema) => {
    return Promise.resolve(user)
  })
  .then((user) => {
    return newUser.signToken(user._id)
  })
  .then((token) => {
    return newUser.sendToken(token)
  })
  .then((response) => {
    return response
  })
  .catch((e) => {
    throw new Error(e)
  })
}

export default signIn