import { SignIn } from '../SignIn/SignIn'
import SaveUser from '../SaveUser'

class SignUp extends SignIn {
  constructor (email: string, screenName: string) {
    super(email, screenName)
  }

  async save () {
    return this.checkExisting()
    .then((res) => {
      return Promise.reject('User already exists.')
    })
    .catch(async (e) => {
      try {
        const user = await SaveUser(this.email, this.screenName)
        return user
      } catch (e) {
        throw e
      }
    })
  }
}

const signUp = async (email: string, screenName: string) => {
  const newUser = new SignUp(email, screenName)
  return newUser.save()
  .then((res) => {
    return Promise.resolve(res)
  })
  .then((user) => {
    return newUser.signToken(user._id)
  })
  .then((token) => {
    return newUser.sendToken(token)
  })
  .then((response) => {
    return response
  })
  .catch((e) => {
    throw e
  })
}

export default signUp