import User from '../../models/User/User'
import { Document, Model } from 'mongoose'
import { UserTypes } from '../../compiler/types'

const saveUser = async (email: string, screenName: string): Promise<UserTypes.UserSchema> => {
  const newUser: UserTypes.UserSchema = new User({
    email, screenName
  })
  const user = await newUser.save()
  return user
}

export default saveUser