import User from '../../models/User/User'

const findUserByEmail = async (email) => {
  try {
    const user = User.findOne({
      email: email
    })
    return user
  } catch (e) {
    throw e
  }
}

export default findUserByEmail