import User from '../../models/User/User'
import { Document } from 'mongoose'
import { UserTypes } from '../../compiler/types'

const checkDuplicateUser = async (email: string, screenName: string) => {
  try {
    const user: UserTypes.UserSchema =  await User.findOne({
      $or: [
        { email: email },
        { screenName: screenName }
      ]
    })
    if (user.email === email && user.screenName === screenName) {
      return true
    } else if (user.email === email) {
      return 'email'
    } else if (user.screenName === screenName) {
      return 'screenName'
    } else {
      return { status: false }
    }
  } catch (e) {
    return { e, status: false }
  }
}

export default checkDuplicateUser