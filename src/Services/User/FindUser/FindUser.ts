import User from '../../../models/User/User'

const findUser = async (email?: string, screenName?: string) => {
  try {
    const user = await User.findOne({
      $or: [
        { email },
        { screenName }
      ]
    })

    if (!user) { throw new Error(`User no existo :/`) }
    return user
  } catch (e) {
    throw e
  }
}


export default findUser