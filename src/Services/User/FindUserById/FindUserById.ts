import User from '../../../models/User/User'

const findUserById = async (id) => {
  const user = await User.findById(id)
  return user
}

export default findUserById