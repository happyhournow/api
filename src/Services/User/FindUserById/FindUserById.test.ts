import FindUserById from './FindUserById'
import { UserTypes } from '../../../compiler/types'
import User from '../../../models/User/User'

const seed = {
  id: '5a5bc6b7734d1d3471836ad4'
}

test('Finds a user by ID', async () => {
  try {
    const user = await User.findById(seed.id)
    expect(user.screenName).toEqual('theomjones')
  } catch (e) {
    fail(e)
  }
})