import { PlaceSchema } from './../../../compiler/types'
import Place from '../../../models/Place/Place'
import { Document } from 'mongoose'


const addPlace = async (name, googlePlaceId) => {
  const place: Document = new Place({
    name,
    googlePlaceId

  })
  try {
    const savedDoc = await place.save()
  } catch (e) {
    throw e
  }
}

export default addPlace