import { Request, Response } from 'express'
import VerifyToken from '../../Services/Authentication/VerifyToken'

const authenticate = (req?: Request, res?: Response, next?: any) => {
    const token: any = req.headers['x-auth']
    VerifyToken(token)
    .then((decoded) => {
      next()
    })
    .catch((e) => {
      res.status(401).send({ message: 'Unauthorised' })
    })
}

export default authenticate