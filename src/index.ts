import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as morgan from 'morgan'
const cors = require('cors')

// Local
const config = require('./config/config')

// Router
import placesRoutes from './routes/placesRoutes'
import userRoutes from './routes/User/UserRoutes'

const port = process.env.PORT

const app = express()

app.use(cors())
app.use(morgan('combined'))
app.use(bodyParser.json())

// Add routes
app.use('/places', placesRoutes)
app.use('/user', userRoutes)



app.listen(port, () => {
  console.log(`API on ${port}...`)
})
