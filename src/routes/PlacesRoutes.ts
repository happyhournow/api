import * as express from 'express'
const router = express.Router()
const cors = require('cors')

// Controllers
import placesController from '../controllers/Places/PlacesController'
import PlaceController from '../controllers/Place/PlaceController'

// Middleware
import authenticate from '../Middleware/Authenticate/Authenticate'
import { PlaceTypes } from '../compiler/types'

// Get Places
router.get('/', cors(), (req, res) => {
  placesController(req, res)
})

// Get Place
router.get('/:id', cors(), authenticate, (req, res) => {
  const ctl = new PlaceController(req, res)
  ctl.getPlace()
})

// Post (Add) Place
// TODO , big job
// router.post('/place', (req: express.Request, res: express.Response) => {
//   const name: string = req.body.name
//   const days: PlaceTypes.Day[] = req.body.days

// })

export default router