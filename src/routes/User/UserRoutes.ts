import * as express from 'express'
import * as bodyParser from 'body-parser'

// Controllers
// import UserController from '../../controllers/User/UserController'
import SignInController from '../../controllers/User/SignInController/SignInController'
import SignUpController from '../../controllers/User/SignUpController/SignUpController'
import AuthenticateController from '../../controllers/Auth/AuthenticateController'

const router = express.Router()
router.use(bodyParser.json())


// url/user | Add User
router.post('/', (req: express.Request, res: express.Response) => {
  SignUpController(req, res)
})

// SignIn
router.post('/signin', (req: express.Request, res: express.Response) => {
  SignInController(req, res)
})

// Authenticate (When you have a token)
router.get('/authenticate', (req: express.Request, res: express.Response) => {
  AuthenticateController(req, res)
})

export default router