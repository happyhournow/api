import { Request, Response } from 'express'
import GetPlace from '../../models/Place/methods/GetPlace'

class PlaceController {
  req: Request
  res: Response
  constructor (req: Request, res: Response) {
    this.req = req
    this.res = res
  }
  public getPlace () {
    const lat: number = parseFloat(this.req.query.lat)
    const lng: number = parseFloat(this.req.query.lng)
    GetPlace(this.req.params.id, this.req.query.unit, this.req.query.mode, { lat: lat, lng: lng })
    .then((place) => {
      this.res.status(200).send(place)
    })
    .catch((err) => {
      this.res.status(404).send({error: err})
    })
  }
}

export default PlaceController