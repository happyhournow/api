import { Request, Response } from 'express'
import signUp from '../../../Services/User/SignUp/SignUp'

const SignUpController = (req: Request, res: Response) => {
  if (!req.body.email && !req.body.screenname) {
    res.status(400).send({ error: 'Please provide an email and screen name.' })
    return
  }
  const email: string = req.body.email
  const screenName: string = req.body.screenname

  signUp(email, screenName)
  .then((response) => {
    res.status(200).send({ success: true, response: response })
  })
  .catch((e) => {

    if (e.code === 11000) {
      res.status(400).send({ success: false, message: 'Username or email already exists' })
    } else {
      res.status(404).send({ success: false, message: e.toString() })
    }
  })

}

export default SignUpController