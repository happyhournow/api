import * as bodyParser from 'body-parser'

// Local
import saveUser from '../../Services/User/SaveUser'
import hashPassword from '../../Services/Authentication/HashPassword'
import User from '../../models/User/User'
import checkDuplicateUser from '../../Services/User/CheckDuplicateUser'
import SaveUser from '../../Services/User/SaveUser'
import { Request, Response } from 'express'
import SignToken from '../../Services/Authentication/SignToken'


// url/user
const UserController = (req: Request, res: Response) => {

  if (!req.body.password || !req.body.name || !req.body.email) {
    return res.status(400).send({ message: 'You must provide a screen name, password and email address.' })
  }

  const email = req.body.email
  const screenName = req.body.name.toLowerCase()
  const plainPassword = req.body.password
  let hashedPassword: string

  // Check if user exists
  // If they do, 400 out
  checkDuplicateUser(email, screenName)
  .then((duplicate) => {
    if (duplicate === true) {
      res.status(400).send({ message: 'User already exits' })
      return
    }
    if (duplicate === 'email') {
     res.status(400).send({ message: 'An account with that email already exists.' })
     return
    } else if (duplicate === 'screenName') {
      res.status(400).send({ message: 'That screen name is taken, sorry. Try another.' })
      return
    } else {
      // No duplicates
      // Otherwise Hash password
       hashPassword(plainPassword)
      .then((hash) => {
        hashedPassword = hash

        SaveUser(email, screenName)
        .then((doc) => {
          // Send token.
          SignToken(doc._id)
          .then((token) => {
            res.status(200).send({
              success: true,
              token: token,
              message: `Signed up ${doc.screenName}! Yay!`
            })
          })
        })
        .catch((e) => {
          console.log(e)
          res.status(500).send({ error: e })
          throw new Error(e)
        })
      })
      .catch((e) => {
        res.status(500).send({ error: e.message, message: 'Must provide a password.' })
        console.log(e)
        // Deal with error
      })
    }
  }).catch((e) => {
    console.log(e)
    res.status(500).send({
      error: e, message: 'Internal server error. 500'
    })
  })
}

export default UserController