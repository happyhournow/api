import { Request, Response } from 'express'
import signIn from '../../../Services/User/SignIn/SignIn'

const SignInController = (req: Request, res: Response) => {
  if (!req.body.email && !req.body.screenname) {
    res.status(400).send({ error: 'Please provide an email or screen name.' })
    return
  }
  const email: string = req.body.email || undefined
  const screenName: string = req.body.screenname || undefined

  signIn(email, screenName)
  .then((response) => {
    res.status(200).send({ success: true, response: 'Go check your emails!' })
  })
  .catch((e) => {
    res.status(404).send({ success: false, message: e.toString() })
  })

}

export default SignInController