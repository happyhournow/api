import { Request, Response } from 'express'
import verifyToken from '../../Services/Authentication/VerifyToken'
import signToken from '../../Services/Authentication/SignToken'

// when a user follows the link in the email, they go to the /user/authenticate endpoint
// url query ?token= is a short life token.
// This Token needs to be verified
// If ok...
  // A new token needs to be signed
  // Send back the new token via json

const authenticateUser = async (req: Request, res: Response) => {
  if (!req.query.token) return res.status(400).json({ success: false, message: 'No token was provided.' })
  const oldToken: string = req.query.token
  try {
     const decodedToken = await verifyToken(oldToken)
     const uid: string = decodedToken.uid
     const newToken = await signToken(uid)
     res.status(200).json({ success: true, token: newToken })
  } catch (e) {
    res.status(400).json({ success: false, message: e })
  }
}

export default authenticateUser