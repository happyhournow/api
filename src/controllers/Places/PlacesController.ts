import { Geo } from '../../compiler/types'
import { Request, Response } from 'express'
import GetPlaces from '../../models/Place/methods/GetPlaces'
import getNearest from '../../models/Place/methods/GetNearest'
import getNearestSpatial from '../../models/Place/methods/GetNearestSpatial'

const placesController = (req: Request, res: Response) => {
  const xToken = req.header('x-auth')
  if (Object.keys(req.query).length > 0 && req.query.unit && req.query.mode && req.query.maxdistance) {
    const lng: number = parseFloat(req.query.lng)
    const lat: number = parseFloat(req.query.lat)
    const userLocation: Geo.Coords = { lng, lat }
    const unit = req.query.unit.toLowerCase().trim()
    const mode = req.query.mode.toLowerCase().trim()
    const maxDistance: number = parseInt(req.query.maxdistance) || 10000

    getNearestSpatial(unit, mode, userLocation, maxDistance)
    .then((result) => {
      res.status(200).send(result)
    })
    .catch((e) => {
      if (e.message === 'MongoError: Legacy point is out of bounds for spherical query') {
        res.status(400).send('Latitude or longitude are invalid')
      } else {
        res.status(404).send({ message: 'Unable to get results', error: e })
      }
    })
  } else {
    GetPlaces()
    .then((result) => {
      res.status(200).send(result)
    })
    .catch((err) => {
      res.status(404).send({ message: 'Could not get places...', err: err})
    })
  }
}

export default placesController