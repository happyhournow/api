const print = (data) => {
  console.log(JSON.stringify(data, undefined, 2))
}

export default print